<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$colleges = array('TCT' => 'Toán cơ Tin Học', 'DL' => 'Địa lý', 'S' => 'Sinh', 'MT' => 'Môi trường');
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
    <title>Day 04: Đăng ký tân sinh viên</title>
</head>
<body>
    <form method="POST" action="">
		<div class="box-error-message">
			<?php
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					if (!isset($_POST["username"]) || empty($_POST["username"])) {
						echo "<p style=\"color: red\">Hãy nhập tên</p>";
					}

					if (!isset($_POST["gender"])) {
						echo "<p style=\"color: red\">Hãy chọn giới tính</p>";
					}
					if (!isset($_POST["college "]) || empty($_POST["college "])) {
						echo "<p style=\"color: red\">Hãy chọn phân khoa</p>";
					}
					if (!isset($_POST["dob"]) || empty($_POST["dob"])) {
						echo "<p style=\"color: red\">Hãy nhập ngay sinh</p>";
					}else {
						if (preg_match("/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/", $_POST["dob"]) === 0) {
							echo "<p style=\"color: red\">Hãy nhập ngay sinh đúng định dạng</p>";
						}
					}
				}
			?>
		</div>
        <div class="m-row">
            <div class="label">
                <label for="name" class="required">Họ và tên</label>
            </div>
            <input class="text-input" type="text" id="username" name="username">
        </div>
        <div class="m-row">
            <div class="label">
                <label for="gender" class="required">Giới tính</label>
            </div>
			<div class="gender-div">
				<?php
					for ($i = 0; $i < count($genders); ++$i) {
						echo ("
							<div>
								<input type=\"radio\" name=\"gender\" value=\"$i\">
								<label>$genders[$i]</label>
							</div>
						");
					}
				?>
			</div>
        </div>
		<div class="m-row">
            <div class="label">
                <label for="college " class="required">Phân khoa</label>
            </div>
			<select name="college " id="college ">
				<?php
					echo "<option></option>";
					foreach ($colleges as $college ) {
						echo "<option value=\"$college \">$college </option>";
					}
				?>
			</select>
        </div>
		<div class="m-row">
			<div class="label">
				<label for="dob" class="required">Ngày sinh</label>
			</div>
			<input class="date-picker" type="text" id="dob" name="dob" placeholder="dd/mm/yyyy">
		</div>
		<div class="m-row">
			<div class="label">
				<label for="address">Địa chỉ</label>
			</div>
			<input class="text-input" type="text" id="address" name="address">
		</div>
        <div class="submit-div">
            <input type="submit" value="Đăng ký"></input>
        </div>
    </form>
	
	<script>
		$(document).ready(function() {
			$(function() {
				$("#dob").datepicker({
					dateFormat: 'dd/mm/yy'
				});
			});
		})
	</script>
	<style>
	form {
		border: 1px solid #82a2be; 
		width: 500px; 
		margin: 0 auto;
		padding: 20px 10px;
	}
	.box-error-message{
		padding-left: 60px;
	}
	
	.required::after {
		content: "*";
		margin-left: 2px;
		color: red;
	}

	.m-row {
		margin: 10px auto;
		width: 410px;
		display: flex;
	}
	.label {
		text-align: center;
		color: white;
		width: 100px;
		border: 1px solid blue;
		background-color: forestgreen;
		padding: 5px;
		margin-right: 20px;
	}

	.submit-div {
		text-align: center;
		margin-top: 20px;
	}

	.gender-div, .gender-div > div {
		display: flex;
		flex-direction: row;
		gap: 10px;
		align-items: center;
	}

	.text-input {
		width: 280px;
		border: 1px solid blue;
		outline: none;
		padding-left: 8px;
	}

	.date-picker {
		border: 1px solid blue;
		width: 150px;
		outline: none;
		padding-left: 8px;
	}

	select {
		border: 1px solid blue;
		width: 150px;
	}

	input[type="submit"] {
		color: white;
		padding: 10px 25px;
		border: 1px solid blue;
		border-radius: 5px;
		background-color: forestgreen;
	}

	input[type='radio']:after {
		content: '';
		width: 15px;
		height: 15px;
		border-radius: 15px;
		top: -5px;
		left: -1px;
		position: relative;
		background-color: forestgreen;
		display: inline-block;
		visibility: visible;
		border: 1px solid blue;
		}

	input[type='radio']:checked:after {
		content: '';
		width: 15px;
		height: 15px;
		border-radius: 15px;
		top: -5px;
		left: -1px;
		position: relative;
		background-color: palevioletred;
		display: inline-block;
		visibility: visible;
		border: 1px solid blue;
	}
	input[type="date"]::-webkit-inner-spin-button, input[type="date"]::-webkit-calendar-picker-indicator {
		display: none;
		-webkit-appearance: none;
	}
	</style>
</body>
</html>
